# Overview
Summary of the capabilities and features
## GitLab References
### Docs
### Handbook
### Videos
### What's on the roadmap
 <!--- Use search parameters to link to related issues--->
- [ ](Feature Release Example)  <xx.xx>

## GitLab Hands-on
### Examples
### Projects
### Templates

## Training
### FAQs
### Subject Matter Experts(SMEs)
SMEs are identified as experts in this topic. They also co-author content and provide editorial guidance. 

## External References

<!-- Issue Metadata -->

/label ~"reference" 
